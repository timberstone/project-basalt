#!/bin/bash

# Script deploying all serverless.yml packages to AWS
# TODO deal with environments
# execute this using source ./deploy.sh

# expects "/path/to/input/file.txt" as parameter
function export_env_vars(){
    file="$1"
    while IFS='' read -r LINE || [ -n "${LINE}" ]; do
        IFS='='
        read -ra VAR <<< "${LINE}" 
        echo "processing line: key=${VAR[0]} ; value=${VAR[1]}"
        export "${VAR[0]}"="${VAR[1]}"
    done < ${file}
}

function check_requirements(){
    if ! command -v aws &> /dev/null
    then
        echo "aws-cli could not be found."
    exit
fi
}

function deploy(){
    package="$1"
    cd packages/${package}

    if [ $RESULT -eq 0 ]; then
        if [ -e .env.dev ]; then
            echo "env vars!"
            # export_env_vars "packages/${package}/.env.dev"
        fi
        echo "Deploying ${package}"
        # sls deploy -c packages/${package}/serverless.yml
    else
        echo "Failed deploying ${package}"
        exit 1 
    fi

    cd ../../

}

function main(){
    echo "Starting deployment script"
    # check_requirements
    # export AWS_PAGER=""

    RESULT=$?
    # echo $HOME
    deploy "auth-api"
    deploy "auth-web"
    deploy "api"
    deploy "authorizer"
    deploy "web"


    # TODO Add the rest of the packages 

}

main "$@"
